package com.simple.curd.productservice.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.simple.curd.productservice.entity.Product;
import com.simple.curd.productservice.repository.ProductRepository;

@RestController
@CrossOrigin
public class ProductController {
	private ProductRepository productRepository;
	
	public ProductController(ProductRepository productRepository)
	{
		this.productRepository = productRepository;
	}
	
	@GetMapping("/products")
	public List<Product> getProducts(){
		return (List<Product>) productRepository.findAll();
	}
	
	@GetMapping("/products/{id}")
	public Optional<Product> getProduct(@PathVariable Long id){
		return productRepository.findById(id);
	}
	
	@PostMapping("/product")
	public void addProduct(@RequestBody Product product){
		productRepository.save(product);
	}
	
	

}
