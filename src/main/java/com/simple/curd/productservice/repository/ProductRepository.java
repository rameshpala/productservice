package com.simple.curd.productservice.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.simple.curd.productservice.entity.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

}
